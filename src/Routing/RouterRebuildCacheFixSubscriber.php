<?php

namespace Drupal\menu_router_rebuild_cache_fix\Routing;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RouterRebuildCacheFixSubscriber.
 *
 * Clears the route match cache after menu links are rebuilt.
 * This is to workaround a tricky core issue documented here:
 * https://www.drupal.org/project/drupal/issues/3075675.
 */
class RouterRebuildCacheFixSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public function onRouterFinished() {
    Cache::invalidateTags(['route_match']);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    // Set priority just after MenuRouterRebuildSubscriber, which has the
    // code that could screw up the route data.
    $events[RoutingEvents::FINISHED][] = ['onRouterFinished', 99];
    return $events;
  }

}

?>
